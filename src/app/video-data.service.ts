import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from './dashboard/interfaces';
import { map } from 'rxjs/operators';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  load(): Observable<Video[]> {
    const url = apiUrl + '/videos';
    return this.http.get<Video[]>(url).pipe(
      map(videos => {
        return videos
          // filter the array to show only a few
          .filter(video => video.title.startsWith('Angular'))
          // change each value of the array
          .map(video => {
            return {
              ...video,
              title: video.title.toUpperCase()
            };
          });
      }));
  }

  getVideo(id: string): Observable<Video> {
    const url = apiUrl + '/videos/' + id;
    return this.http.get<Video>(url);
  }
}
