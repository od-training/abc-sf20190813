import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../interfaces';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  videoId: Observable<string>;

  constructor(ar: ActivatedRoute) {
    this.videoId = ar.queryParamMap.pipe(
      map(params => params.get('videoId'))
    );
  }
}
