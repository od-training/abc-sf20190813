import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { map, switchMap } from 'rxjs/operators';
import { initializeVideos, videoListArrived } from '../state';
import { VideoDataService } from '../video-data.service';


@Injectable()
export class DashboardEffects implements OnInitEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly svc: VideoDataService
  ) {
  }

  init$ = createEffect(() => this.actions$
    .pipe(
      ofType(initializeVideos),
      switchMap(() => this.svc.load()),
      map(videos => videoListArrived({ videos }))
    )
  );

  ngrxOnInitEffects(): Action {
    return initializeVideos();
  }
}
