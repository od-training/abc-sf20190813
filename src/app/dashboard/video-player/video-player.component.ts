import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Video } from '../interfaces';
import { map, switchMap, filter } from 'rxjs/operators';
import { VideoDataService } from 'src/app/video-data.service';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  video: Observable<Video>;
  url: Observable<SafeUrl>;

  constructor(ar: ActivatedRoute, svc: VideoDataService, sanitizer: DomSanitizer) {
    this.video = ar.queryParamMap.pipe(
      map(params => params.get('videoId')),
      filter(id => !!id),
      switchMap(id => svc.getVideo(id))
    );
    this.url = this.video.pipe(
      map(video => sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video.id))
    );
  }
}
