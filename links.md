# Angular Boot Camp

[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager](http://videomanager.angularbootcamp.com)

[Workshop Repository](https://bitbucket.org/od-training/abc-sf20190813)

[Sample Video Data](https://api.angularbootcamp.com/videos)

[Survey](https://angularbootcamp.com/survey)

# Resources

[ExploringJS](https://exploringjs.com/es6/index.html)

[TypeScript Handbook](http://www.typescriptlang.org/docs/handbook/basic-types.html)

[Domain Converter from Cory Rylan](https://stackblitz.com/edit/typescript-domain-converter)

[Component Interactions](https://angular.io/guide/component-interaction)

[Dynamic Form Generator](https://github.com/formql/formql)

["Crying Baby" video](https://www.youtube.com/watch?v=eBLTz8QRg4Q)

## Observables

[Cory Rylan Video on Observables](https://www.youtube.com/watch?v=-yY2ECd2tSM)

[Book: Build Reactive Websites with
RxJS](https://pragprog.com/book/rkrxjs/build-reactive-websites-with-rxjs)

[I switched a map and you'll never guess what happened next (SwitchMap
video)](https://www.youtube.com/watch?v=rUZ9CjcaCEw)

[RXMarbles](http://rxmarbles.com)

[LearnRXJS](https://www.learnrxjs.io/)

[Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)

[Reactive Visualizations](https://reactive.how/)

[Operator Decision Tree](https://rxjs-dev.firebaseapp.com/operator-decision-tree)

## VSCode Extensions

[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[angular2-inline](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline)

[angular2-switcher](https://marketplace.visualstudio.com/items?itemName=infinity1207.angular2-switcher)

[Bracket Pair Colorizer
2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

## Conferences

[Angular Conferences](https://angularconferences.com/)

## Podcasts

[Angular Air (Videocast and Podcast)](https://angularair.com/)

[Adventures in Angular (Podcast)](https://devchat.tv/adv-in-angular)

[Real Talk JavaScript](https://realtalkjavascript.simplecast.fm/)

## Newsletters

[ng-newsletter (weekly Angular email)](https://www.ng-newsletter.com/)

[Angular Top 5 (weekly Angular email)](http://angulartop5.com/)

## Blogs

[Angular In Depth](https://blog.angularindepth.com/)

[Netanel Basal](https://netbasal.com/)

[Cory Rylan](https://coryrylan.com/)
